def get_sequence():
    sequence = ''
    symbols = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
        'u', 'v', 'w', 'x', 'y', 'z'
    ]
    i = 0

    while i < 1000000:
        for s in symbols:
            for s1 in symbols:
                for s2 in symbols:
                    i += 3
                    sequence = sequence + s
                    if i > 1000000:
                        break
                    sequence = sequence + s1
                    sequence = sequence + s2
                if i > 1000000:
                    break
            if i > 1000000:
                break

    return sequence


result = get_sequence()

single = {}
pairs = {}
trios = {}

for i in range(len(result)-2):
    if result[i] not in single:
        single[result[i]] = 0
    single[result[i]] += 1
    new_pair = result[i:i+2]
    if new_pair not in pairs:
        pairs[new_pair] = 0
    pairs[new_pair] += 1
    new_trio = result[i:i+3]
    if new_trio not in trios:
        trios[new_trio] = 0
    trios[new_trio] += 1