import asyncio
import functools
import signal
from concurrent.futures import CancelledError


class BasicTrafficLight:
    def __init__(self,):
        self._green = False
        self._red = False
        self.timer = None

    @property
    def is_green(self):
        return self._green

    def green_on(self):
        self._green = True

    def green_off(self):
        self._green = False

    @property
    def is_red(self):
        return self._red

    def red_on(self):
        self._red = True

    def red_off(self):
        self._red = False

    def turn_off(self):
        self.red_off()
        self.green_off()

    def hardware_check(self):
        errors = None  # check_errors(self)
        if not errors:
            return True
        else:
            return errors

    def state(self):
        return f'red is on - {str(self.is_red)}, green is on - {str(self.is_green)}, timer - {self.timer}'


class CarTrafficLight(BasicTrafficLight):
    def __init__(self,):
        self._yellow = False
        super(CarTrafficLight, self).__init__()

    @property
    def is_yellow(self):
        return self._yellow

    def yellow_on(self):
        self._yellow = True

    def yellow_off(self):
        self._yellow = False

    def turn_off(self):
        self.yellow_off()
        super(CarTrafficLight, self).turn_off()

    def state(self):
        return f'red is on - {str(self.is_red)}, yellow is on - {str(self.is_yellow)}, green is on - {str(self.is_green)}, timer - {self.timer}'


class AutoController:
    """
    Simple two-period traffic light controller for X or T type crossroad
    Period deltas are used to set continium of green time periods and balancing traffic flows
    """
    await_period = 5

    def __init__(self, first_period_pedestrian_lights, first_period_car_lights,
                 second_period_pedestrian_lights, second_period_car_lights,
                 first_period_delta, second_period_delta):
        self.first_period_car_lights = set(first_period_car_lights)
        self.first_period_pedestrian_lights = set(first_period_pedestrian_lights)
        self.second_period_pedestrian_lights = set(second_period_pedestrian_lights)
        self.second_period_car_lights = set(second_period_car_lights)
        self.all_lights = self.first_period_pedestrian_lights & self.first_period_car_lights & \
                          self.second_period_pedestrian_lights & self.second_period_car_lights
        errors = []
        for light in self.all_lights:
            light_errors = light.hardware_check()
            if light_errors:
                errors.append((light_errors, light.serial_number))
        if errors:
            for error in errors:
                print(str(error))
            raise RuntimeError

        self.first_period_delta = first_period_delta
        self.second_period_delta = second_period_delta
        self.first_period = False
        self.second_period = False
        self.loop = asyncio.get_event_loop()
        self.automatic_mode = False
        self.test_mode = False
        self.manual_mode = False

    def add_first_period_car_light(self, new_light):
        self.first_period_car_lights.add(new_light)

    def add_second_period_car_light(self, new_light):
        self.second_period_car_lights.add(new_light)

    def add_first_period_pedestrian_light(self, new_light):
        self.first_period_pedestrian_lights.add(new_light)

    def add_second_period_pedestrian_light(self, new_light):
        self.second_period_car_lights.add(new_light)

    def remove_first_period_car_light(self, light):
        light.red_off()
        light.green_off()
        light.yellow_off()
        self.first_period_car_lights.remove(light)

    def remove_second_period_car_light(self, light):
        light.red_off()
        light.green_off()
        light.yellow_off()
        self.second_period_car_lights.remove(light)

    def remove_first_period_pedestrian_light(self, light):
        light.red_off()
        light.green_off()
        light.yellow_off()
        self.first_period_pedestrian_lights.remove(light)

    def _second_period_pedestrian_light(self, light):
        light.red_off()
        light.green_off()
        light.yellow_off()
        self.second_period_car_lights.remove(light)

    def start_automatic_mode(self):
        self.automatic_mode = True
        self.manual_mode = False
        # switching from automatic to manual mode possible with external 'SIGTERM' signal
        self.loop.add_signal_handler(
            signal.SIGTERM,
            functools.partial(self.term_handler),
        )
        try:
            self.loop.run_until_complete(
                asyncio.gather(
                    self.main_coroutine(),
                    self.first_period_car_timer(),
                    self.second_period_car_timer(),
                    self.first_period_pedestrian_timer(),
                    self.second_period_pedestrian_timer()
                               )
            )
        except CancelledError:
            self.manual_mode = True

    async def term_handler(self):
        self.automatic_mode = False

    def stop_loop_tasks(self):
        for task in asyncio.Task.all_tasks():
            task.cancel()
        for light in self.all_lights:
            light.timer = None

    def start_in_manual_mode(self):
        self.manual_mode = True
        self.manual_mode_switch()

    def manual_mode_switch(self):
        if not self.manual_mode:
            raise RuntimeError('Manual switching possible only in manual mode!')
        if not self.second_period and not self.first_period:
            for light in self.all_lights:
                light.red_on()
            self.loop.run_until_complete(
                asyncio.gather(
                    self.first_street_go()
                               )
            )
        if self.first_period and not self.second_period:
            self.loop.run_until_complete(
                asyncio.gather(
                    self.first_street_stop()
                               )
            )
            self.loop.run_until_complete(
                asyncio.gather(
                    self.second_street_go()
                               )
            )
        if not self.first_period and self.second_period:
            self.loop.run_until_complete(
                asyncio.gather(
                    self.second_street_stop()
                               )
            )
            self.loop.run_until_complete(
                asyncio.gather(
                    self.first_street_go()
                               )
            )

    def stop_traffic(self):
        """
        Emergency traffic stop
        """
        if not self.second_period and not self.first_period:
            for light in self.all_lights:
                light.red_on()
        if self.first_period and not self.second_period:
            self.loop.run_until_complete(
                asyncio.gather(
                    self.first_street_stop()
                               )
            )
        if not self.first_period and self.second_period:
            self.loop.run_until_complete(
                asyncio.gather(
                    self.second_street_stop()
                               )
            )

    async def first_street_go(self):
        self.second_period = False
        self.first_period = True
        for light in self.first_period_car_lights:
            light.yellow_on()
            light.red_off()
        await asyncio.sleep(self.await_period)
        for light in self.first_period_car_lights:
            light.green_on()
            light.yellow_off()
        for light in self.first_period_pedestrian_lights:
            light.green_on()
            light.red_off()

    async def first_street_stop(self):
        for light in self.first_period_car_lights:
            light.yellow_on()
            light.green_off()
        for light in self.first_period_pedestrian_lights:
            light.red_on()
            light.green_off()
        await asyncio.sleep(self.await_period)
        for light in self.first_period_car_lights:
            light.red_on()
            light.yellow_off()

    async def second_street_go(self):
        self.second_period = True
        self.first_period = False
        for light in self.second_period_car_lights:
            light.yellow_on()
            light.red_off()
        await asyncio.sleep(self.await_period)
        for light in self.second_period_car_lights:
            light.green_on()
            light.yellow_off()
        for light in self.second_period_pedestrian_lights:
            light.green_on()
            light.red_off()

    async def second_street_stop(self):
        for light in self.second_period_car_lights:
            light.yellow_on()
            light.green_off()
        for light in self.second_period_pedestrian_lights:
            light.red_on()
            light.green_off()
        await asyncio.sleep(self.await_period)
        for light in self.second_period_car_lights:
            light.red_on()
            light.yellow_off()

    def report(self, ):
        new_line = '\n'
        result = f'''
        First Period Car Lights:
        {new_line.join([light.state() for light in self.first_period_car_lights ])}
        First Period Pedestrian Lights:
        {new_line.join([light.state() for light in self.first_period_pedestrian_lights ])}
        Second Period Car Lights:
        {new_line.join([light.state() for light in self.second_period_car_lights ])}
        Second Period Pedestrian Lights:
        {new_line.join([light.state() for light in self.second_period_pedestrian_lights ])}
        '''
        return result

    async def main_coroutine(self):
        for light in self.all_lights:
            light.red_on()
        while True:
            # wait for full stop on the second street
            await asyncio.sleep(self.await_period)
            # safe time to switch modes
            if not self.automatic_mode:
                self.stop_loop_tasks()
            # go for first street
            self.second_period = False
            self.first_period = True
            for light in self.first_period_car_lights:
                light.yellow_on()
                light.red_off()
            await asyncio.sleep(self.await_period)
            for light in self.first_period_car_lights:
                light.green_on()
                light.yellow_off()
            for light in self.first_period_pedestrian_lights:
                light.green_on()
                light.red_off()
            await asyncio.sleep(self.first_period_delta)
            # stop fo first street
            for light in self.first_period_car_lights:
                light.yellow_on()
                light.green_off()
            for light in self.first_period_pedestrian_lights:
                light.red_on()
                light.green_off()
            await asyncio.sleep(self.await_period)
            for light in self.first_period_car_lights:
                light.red_on()
                light.yellow_off()
            # wait for full stop on the first street
            await asyncio.sleep(self.await_period)
            # safe time to switch modes
            if not self.automatic_mode:
                self.stop_loop_tasks()
            # go for second street
            self.second_period = True
            self.first_period = False
            for light in self.second_period_car_lights:
                light.yellow_on()
                light.red_off()
            await asyncio.sleep(self.await_period)
            for light in self.second_period_car_lights:
                light.green_on()
                light.yellow_off()
            for light in self.second_period_pedestrian_lights:
                light.green_on()
                light.red_off()
            await asyncio.sleep(self.second_period_delta)
            # stop for second street
            for light in self.second_period_car_lights:
                light.yellow_on()
                light.green_off()
            for light in self.second_period_pedestrian_lights:
                light.red_on()
                light.green_off()
            await asyncio.sleep(self.await_period)
            for light in self.second_period_car_lights:
                light.red_on()
                light.yellow_off()

    async def first_period_car_timer(self):
        await asyncio.sleep(self.await_period)
        while True:
            await asyncio.sleep(self.await_period)
            i = self.first_period_delta
            while i > 0:
                i -= 1
                for light in self.first_period_car_lights:
                    light.timer = i
                await asyncio.sleep(1)
            await asyncio.sleep(self.await_period)
            i = self.second_period_delta + self.await_period * 4
            while i > 0:
                i -= 1
                for light in self.first_period_car_lights:
                    light.timer = i
                await asyncio.sleep(1)

    async def second_period_car_timer(self):
        while True:
            await asyncio.sleep(self.await_period)
            i = self.first_period_delta + self.await_period * 4
            while i > 0:
                i -= 1
                for light in self.second_period_car_lights:
                    light.timer = i
                await asyncio.sleep(1)
            await asyncio.sleep(self.await_period)
            i = self.second_period_delta
            while i > 0:
                i -= 1
                for light in self.second_period_car_lights:
                    light.timer = i
                await asyncio.sleep(1)

    async def first_period_pedestrian_timer(self):
        await asyncio.sleep(self.await_period * 2)
        while True:
            i = self.first_period_delta
            while i > 0:
                i -= 1
                for light in self.first_period_pedestrian_lights:
                    light.timer = i
                await asyncio.sleep(1)
            i = self.second_period_delta + self.await_period * 6
            while i > 0:
                i -= 1
                for light in self.first_period_pedestrian_lights:
                    light.timer = i
                await asyncio.sleep(1)

    async def second_period_pedestrian_timer(self):
        i = self.second_period_delta + self.await_period * 5
        while i > 0:
            i -= 1
            for light in self.second_period_pedestrian_lights:
                light.timer = i
                await asyncio.sleep(1)

        while True:
            i = self.second_period_delta
            while i > 0:
                i -= 1
                for light in self.second_period_pedestrian_lights:
                    light.timer = i
                await asyncio.sleep(1)
            i = self.first_period_delta + self.await_period * 6
            while i > 0:
                i -= 1
                for light in self.second_period_pedestrian_lights:
                    light.timer = i
                await asyncio.sleep(1)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop_traffic()
        self.loop.close()


if __name__ == "__main__":
    b = BasicTrafficLight()
    c = CarTrafficLight()
    b1 = BasicTrafficLight()
    c1 = CarTrafficLight()
    with AutoController([b], [c], [b1], [c1], 10, 20) as ctrl:
        ctrl.start_automatic_mode()
