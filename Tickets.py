def tickets(input_data):
    """
    :param input_data: tuple with 2 variables
    s: sum of ALL digits in ticket number
    n: half of number length
    :return:
    """
    n, s = input_data

    array = [1] * 10 + [0] * (n * 9 - 9)
    for i in range(n - 1):
        array = [sum(array[x::-1]) if x < 10 else sum(array[x:x - 10:-1]) for x in range(len(array))]

    return array[s//2]**2
